package com.gabriel_jonas.leaf_segmentation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SelectActivity extends AppCompatActivity {
    public static final int IMAGE_GALLERY_REQUEST = 20;
    public static final int CAM_REQUEST = 1313;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public int orientationImg;
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }


    public void OnImageGalleryClicked(View v) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();

        System.out.println("Orientação"+orientationImg);
        // finally, get a URI representation
        Uri data = Uri.parse(pictureDirectoryPath);

        // set the data and type.  Get all image types.
        photoPickerIntent.setDataAndType(data, "image/*");

        // we will invoke this activity, and get something back from it.
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);

    }

    private static Uri getOutputMediaFileUri(int type) {
        System.out.println("aqui tbm");
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        System.out.println("aqui será?");
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        System.out.println("OLOCO BIXO");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            System.out.println("nao tem");
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("nao deixa criar");
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        System.out.println("eiiiii");
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }
        System.out.println("hooooooooou");
        return mediaFile;
    }

    public void OnCameraClicked(View v) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        System.out.println("entrou");
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
        System.out.println("voltou");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
        System.out.println("é aqui?");
        // start the image capture Intent
        startActivityForResult(intent, CAM_REQUEST);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // if we are here, everything processed successfully.
            if (requestCode == IMAGE_GALLERY_REQUEST) { //Galeria
                // if we are here, we are hearing back from the image gallery.

                // the address of the image on the SD Card.
                Uri imageUri = data.getData();

                //System.out.println(exifToDegrees(orientationImg));
                // declare a stream to read the image data from the SD Card.
                Bundle b = new Bundle();
                b.putString("imageUri",imageUri.toString());

                Intent intent = new Intent(this, MainActivity.class);
                System.out.println(intent.toString());
                System.out.println("Chegou aqui");
                intent.putExtras(b); //Put your id to your next Intent
                System.out.println("intent");
                startActivity(intent);
                System.out.println("Start activity");
                finish();


                //InputStream inputStream;

                // we are getting an input stream, based on the URI of the image.
                /*try {
                    inputStream = getContentResolver().openInputStream(imageUri);

                    // get a bitmap from the stream.
                    Bitmap image = BitmapFactory.decodeStream(inputStream);
                    System.out.println(image.getWidth()+"--"+image.getHeight());
                    if(image.getWidth() > image.getHeight()){
                        System.out.println("Rodou");
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        image= Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
                    }
                    // show the image to the user

                    //Bitmap image = BitmapFactory.decodeStream(inputStream);
                    Matrix m = new Matrix();
                    m.setRectToRect(new RectF(0, 0, image.getWidth(), image.getHeight()), new RectF(0, 0, 768, 768), Matrix.ScaleToFit.CENTER);
                    image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), m, true);

                    inputStream.close();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    Bundle b = new Bundle();
                    b.putByteArray("photo",byteArray);

                    Intent intent = new Intent(this, MainActivity.class);
                    System.out.println(intent.toString());
                    System.out.println("Chegou aqui");
                    intent.putExtras(b); //Put your id to your next Intent
                    System.out.println("intent");
                    startActivity(intent);
                    System.out.println("Start activity");
                    finish();

                    //call other Activity
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    // show a message to the user indictating that the image is unavailable.
                    Toast.makeText(this, "Unable to open image", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/

            }
            if (requestCode == CAM_REQUEST) { //Camera
                //imgPicture.setImageURI(mHighQualityImageUri);
                System.out.println("ou aqui?");
                /*Toast.makeText(this, "Image saved to:\n" +
                        data.getData(), Toast.LENGTH_LONG).show();*/
                System.out.println("antes uri");
                Uri imageUri = fileUri;
                Bundle b = new Bundle();
                b.putString("imageUri",imageUri.toString());

                Intent intent = new Intent(this, MainActivity.class);
                System.out.println(intent.toString());
                System.out.println("Chegou aqui");
                intent.putExtras(b); //Put your id to your next Intent
                System.out.println("intent");
                startActivity(intent);
                System.out.println("Start activity");
                finish();
                /*
                // declare a stream to read the image data from the SD Card.
                InputStream inputStream;
                try {
                    System.out.println("Dentro do try");
                    inputStream = getContentResolver().openInputStream(imageUri);
                    System.out.println("Antes bitmap");
                    // get a bitmap from the stream.

                    Bitmap image = BitmapFactory.decodeStream(inputStream);
                    //Bitmap b = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
                    Matrix m = new Matrix();
                    m.setRectToRect(new RectF(0, 0, image.getWidth(), image.getHeight()), new RectF(0, 0, 768, 768), Matrix.ScaleToFit.CENTER);
                    image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), m, true);
                    System.out.println("antes do set");

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    Bundle b = new Bundle();
                    b.putByteArray("photo",byteArray);

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                    finish();



                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    // show a message to the user indictating that the image is unavailable.
                    Toast.makeText(this, "Unable to open image", Toast.LENGTH_LONG).show();
                }*/
            }
        }
    }



}
