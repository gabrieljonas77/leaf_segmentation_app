package com.gabriel_jonas.leaf_segmentation;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import 	android.content.CursorLoader;

import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity {
    public static final int IMAGE_GALLERY_REQUEST = 20;
    public static final int CAM_REQUEST = 1313;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public double height = 0;
    public double width = 0;
    public double perimeter = 0;
    public double leaf_area = 0;
    public int orientationImg;
    private Uri fileUri;
    private ImageView imgPicture;
    private AlertDialog alertDialog;
    private TextView name;
    private TextView info1;
    private TextView info2;
    private TextView info3;
    private TextView info4;
    private Button process;
    private Bitmap img_gui;
    boolean processou = false;

    static {
        if (!OpenCVLoader.initDebug()) {
            System.err.println("Não carregou!");
        } else {
            System.err.println("Carregou!");
        }

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Chamou");
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        System.out.println("Criou");
        setContentView(R.layout.activity_main);

        imgPicture = (ImageView) findViewById(R.id.imageView);
        name = (TextView) findViewById(R.id.filename);
        info1 = (TextView) findViewById(R.id.info1);
        info2 = (TextView) findViewById(R.id.info2);
        info3 = (TextView) findViewById(R.id.info3);
        info4 = (TextView) findViewById(R.id.info4);
        process = (Button) findViewById(R.id.processBt);

        Calendar c = Calendar.getInstance();
        int Month = c.get(Calendar.MONTH) + 1;
        String mes;
        if(Month < 10){
            mes = "0"+Integer.toString(Month);
        }else{
            mes = Integer.toString(Month);
        }
        String date = c.get(Calendar.DAY_OF_MONTH)+ "/"+ mes + "/" + c.get(Calendar.YEAR);
        info1.setText(date);
        Bundle b = getIntent().getExtras();
        if(b != null) {
            Uri imageUri = Uri.parse(b.getString("imageUri"));
            String scheme = imageUri.getScheme();
            if (scheme.equals("file")) {
                 name.setText(imageUri.getLastPathSegment());
            }else{
                String[] proj = { MediaStore.Images.Media.TITLE };
                Cursor cursor = this.getApplicationContext().getContentResolver().query(imageUri, proj, null, null, null);
                if (cursor != null && cursor.getCount() != 0) {
                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.TITLE);
                    cursor.moveToFirst();
                    name.setText(cursor.getString(columnIndex));
                }
                if (cursor != null) {
                    cursor.close();
                }
                //name.setText("Error with file name");
            }


            try {
                File f = new File(imageUri.getPath());
                ExifInterface exif = new ExifInterface(f.getAbsolutePath());
                float[] latLong = new float[2];
                boolean hasLatLong = exif.getLatLong(latLong);
                if (hasLatLong) {
                    System.out.println("Latitude: " + latLong[0]);
                    System.out.println("Longitude: " + latLong[1]);
                }

            }catch(Exception e){
                e.printStackTrace();
            }

            img_gui = getImage(imageUri);
            imgPicture.setImageBitmap(img_gui);



        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void Voltar(View v){
        process.setClickable(true);
        Intent intent = new Intent(this, SelectActivity.class);
        startActivity(intent);
        finish();
    }

    public static Bitmap decodeFile(InputStream f, int WIDTH, int HIGHT){
        try {
            //Decode image size

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(f ,null,o);
            System.out.println("uhashuahushua");
            System.out.println(o.outHeight);
            System.out.println(o.outWidth);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            Bitmap image = BitmapFactory.decodeStream(f);
            if(f == null){
                System.out.println("deu erro aqui");

            }else{

                System.out.println("erro em outro lugar");
            }
            if(image == null){
                System.out.println("deu erro aqui2");

            }else{

                System.out.println("erro em outro lugar");
            }
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;

        final int width = options.outWidth;

        info2.setText(width + " x "+height);

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public Bitmap decodeSampledBitmapFromResource(Uri res2, int resId,
                                                        int reqWidth, int reqHeight) {
        InputStream res = null;
        try {
            res = getContentResolver().openInputStream(res2);
        }catch(Exception ex){
            ex.printStackTrace();

        }
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(res, null, options);
        try {
            res.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }

        try {
            res = getContentResolver().openInputStream(res2);
        }catch(Exception ex){
            ex.printStackTrace();

        }
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(res, null, options);
    }



    private Bitmap getImage(Uri imageUri) {
        InputStream inputStream;
        Bitmap image = null;
        try {
            //inputStream = getContentResolver().openInputStream(imageUri);
            File myFile = new File(imageUri.getPath());
            String absolutePath = myFile.getAbsolutePath();
            //imageUri.getPath();
            // get a bitmap from the stream.

            System.out.println("--------TENTOU LER---------");
            //image = BitmapFactory.decodeFile(absolutePath);
            image = decodeSampledBitmapFromResource(imageUri,1,1024,768);
            System.out.println("-----LEU---------");
            /*System.out.println(image.getWidth() + "--" + image.getHeight());
            if (image.getWidth() > image.getHeight()) {
                System.out.println("Rodou");
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
            }*/
            //info2.setText(image.getWidth()+"x"+image.getHeight());
            Matrix m = new Matrix();
            m.setRectToRect(new RectF(0, 0, image.getWidth(), image.getHeight()), new RectF(0, 0, 1024, 1024), Matrix.ScaleToFit.CENTER);
            if(image.getWidth() > image.getHeight()){
                m.postRotate(90);
            }
            image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), m, true);
            //image = decodeFile(inputStream,1280,720);

            //inputStream.close();
        }catch (Exception ex){
            ex.printStackTrace();

        }
        return image;
    }

    /*public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        System.out.println("N "+ExifInterface.ORIENTATION_NORMAL+"H "+ExifInterface.ORIENTATION_FLIP_HORIZONTAL);

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:

                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }*/
   /* public View view_public;
    public void launchRingDialog(View view) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Downloading Image ...", true);
        ringProgressDialog.setCancelable(true);
        view_public = view;
        //final boolean terminou = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Calcula(view_public);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ringProgressDialog.dismiss();
            }
        }).start();
    }*/

    public void shareBitmap (View view) {
        try {
            Bitmap bitmap = ((BitmapDrawable) imgPicture.getDrawable()).getBitmap();
            File file = new File(getExternalCacheDir(), "img_to_send.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(     android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



     /*public void OnImageGalleryClicked(View v) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();

        System.out.println("Orientação"+orientationImg);
        // finally, get a URI representation
        Uri data = Uri.parse(pictureDirectoryPath);

        // set the data and type.  Get all image types.
        photoPickerIntent.setDataAndType(data, "image/*");

        // we will invoke this activity, and get something back from it.
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);

    }

    public void OnCameraClicked(View v) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        System.out.println("entrou");
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
        System.out.println("voltou");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
        System.out.println("é aqui?");
        // start the image capture Intent
        startActivityForResult(intent, CAM_REQUEST);

    }

    private static Uri getOutputMediaFileUri(int type) {
        System.out.println("aqui tbm");
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        System.out.println("aqui será?");
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        System.out.println("OLOCO BIXO");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            System.out.println("nao tem");
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("nao deixa criar");
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        System.out.println("eiiiii");
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }
        System.out.println("hooooooooou");
        return mediaFile;
    }*/
    public Mat bwareaopen(Mat original, int tam)
    {
        //remove regiões com contorno menor que tam. caso tam == -1, deixa apenas a maior região
        Mat trimmed = new Mat(original.rows(), original.cols(), original.type());
        //Highgui.imwrite("bin/00-original.png", original);
        trimmed.setTo(new Scalar(0));
        Mat original_copy = new Mat();
        original.copyTo(original_copy);
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(original_copy, contours, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);
        if(tam == -1)
        {
            //acha o maior
            int maior = 0;
            for(Integer i = 0; i < contours.size(); i++)
            {
                if(contours.get(i).rows() > maior)
                    maior = contours.get(i).rows();
            }
            tam = maior;
        }
        for(Integer i = 0; i < contours.size(); i++)
        {
            if(contours.get(i).rows() >= tam)
                Imgproc.drawContours(trimmed, contours, i, new Scalar(255, 255, 255), 1 );
        }
        //Highgui.imwrite("bin/01-contornos.png", trimmed);
        int row = (int)original.rows()/2;
        int col = 0;
        Imgproc.floodFill(trimmed, new Mat(), new Point(col, row), new Scalar(255));
        //Highgui.imwrite("bin/02-fill.png", trimmed);
        Imgproc.threshold(trimmed, trimmed, 0, 255, Imgproc.THRESH_BINARY_INV);
        //Highgui.imwrite("bin/03-inv.png", trimmed);
        return trimmed;
    }

    public double sd(List<Integer> list,double media){
        double variancia = 0;
        for(Integer i: list){
            System.out.println(i);
            System.out.println(media);
            variancia += Math.pow(i-media,2.0);
        }
        return Math.sqrt(variancia)/list.size();
    }

    public double mean(List<Integer> list){
        double total = 0;
        for(Integer i: list){
            total+=i;
        }
        return total/list.size();
    }

    public void Calcula(View v) {
        if (imgPicture.getDrawable() != null) {
            Bitmap bmp = ((BitmapDrawable) imgPicture.getDrawable()).getBitmap();
            Mat img = new Mat();
            Utils.bitmapToMat(bmp, img);
            Mat original = img.clone();
            //cvThreshold(s, dst, 100, 100, CV_THRESH_BINARY_INV);
            try {
                Mat b, g, r;
                List<Mat> bgr = new ArrayList<Mat>();
                System.out.println("Antes split"+img.channels());
                Core.split(img, bgr);
                b = bgr.get(2);
                Core.add(b,b,b);

                g = bgr.get(1);
                r = bgr.get(0);
                Core.subtract(b,r,b);
                Mat hsv = new Mat();
                System.out.println("Depois split"+img.channels());
                Imgproc.cvtColor(img, hsv, Imgproc.COLOR_RGB2HSV,3);
                System.out.println("Converteu HSV");
                Mat h, s;
                List<Mat> hsv_channels = new ArrayList<Mat>();
                Core.split(hsv, hsv_channels);
                h = hsv_channels.get(0);

                s = hsv_channels.get(1);



                Mat white = new Mat(h.rows(),h.cols(), CvType.CV_8UC1,new Scalar(255));
                Core.subtract(white,h,h);
                Mat ret_h = new Mat(),ret_s = new Mat(),ret_b = new Mat();

                Imgproc.threshold(h,ret_h,0,255,Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgproc.threshold(s,ret_s,0,255,Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                Imgproc.threshold(b,ret_b,0,255,Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);


                Mat bitwise_b_s = new Mat();

                Core.bitwise_and(ret_s,ret_b,bitwise_b_s);

                Mat quadrados = bwareaopen(bitwise_b_s, (int)(bitwise_b_s.cols() * bitwise_b_s.rows()*0.00005));

                //return;


                List <Rect> lista_quadrados = new ArrayList<Rect>();
                Mat quadrados_2 = quadrados.clone();
                Mat quadrados_3 = quadrados.clone();
                System.out.println("Antes do while");
                while (true) {
                    Mat nonZero = new Mat();
                    Core.findNonZero(quadrados_2, nonZero);
                    //saida =
                    if (nonZero.total() <= 0.0) {
                        break;
                    }
                    MatOfPoint mop = new MatOfPoint(nonZero);



                    //quadrados_2 = 255 - quadrados_2
                    Rect quad = new Rect();
                    Core.subtract(white, quadrados_2, quadrados_2);
                    Imgproc.floodFill(quadrados_2,new Mat(),mop.toArray()[0],new Scalar(255),quad,new Scalar(10),new Scalar(10),8);
                    Core.subtract(white,quadrados_2,quadrados_2);
                    lista_quadrados.add(quad.clone());
                    System.out.println("Fim iteracao while");

                }

                if(lista_quadrados.size() != 4) {
                    Context context = getApplicationContext();
                    CharSequence text = "Não foram identificados 4 quadrados!";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    System.out.println("Nao foram identificados 4 quadrados!");
                    return;
                }
                List<Integer> areas = new ArrayList<>();

                int cont = 0;

                int maior_x = 0;
                int maior_y = 0;
                int maior_height = 0;
                int maior_width = 0;
                int menor_x = 999999;
                int menor_y = 999999;
                int bigger_area =0;
                int maior_p = 0;
                //Separando os quadrados!
                for(Rect quad : lista_quadrados){
                    int x = (int) quad.tl().x;
                    int y = (int) quad.tl().y;
                    int width = (int) quad.width;
                    int height = (int) quad.height;

                    if(width > maior_width)
                        maior_width = width;
                    if(height > maior_height)
                        maior_height = height;

                    if(x+width> maior_x)
                        maior_x = x + width;
                    if(x < menor_x)
                        menor_x = x;

                    if(y + height > maior_y)
                        maior_y = y + height;
                    if(y < menor_y)
                        menor_y = y;

                    //print 'rect1 = ',rect
                    System.out.println("X="+x+" Y="+y+" Width="+width+" Height="+height);
                    byte buff[] = new byte[(int)(quadrados_3.total() * quadrados_3.channels())];
                    int pixels_w = 0;
                    Mat black = new Mat(quadrados_3.rows(),quadrados_3.cols(), CvType.CV_8UC1,new Scalar(0));
                    Imgproc.rectangle(black, quad.tl(),quad.br(),new Scalar(255),-1);
                    Core.bitwise_and(quadrados_3,black,black);
                    /*Utils.matToBitmap(black, bmp);
                    imgPicture.setImageBitmap(bmp);
                    if(true){
                        return;
                    }*/
                    pixels_w = Core.countNonZero(black);
                    System.out.println("Pixels brancos"+pixels_w);
                    //print 'pixels_w = ',pixels_w
                    float percentage = (float) ((float)(pixels_w) / (float)(width * height));
                    System.out.println("Rect %"+percentage);
                    if(percentage < 0.8) {
                        Context context = getApplicationContext();
                        CharSequence text = "Imagem Inválida!";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        System.out.println("Imagem Invalida!");
                        return;
                    }


                    bigger_area = bigger_area + width*height;
                    areas.add(width*height);
                    cont +=1;
                }

                //Separando a folha!
                System.out.println("Maior X="+maior_x);
                System.out.println("Maior Y="+maior_y);
                System.out.println("Menor X="+menor_x);
                System.out.println("Mmenor Y="+menor_y);
                System.out.println("Maior Width="+maior_width);
                System.out.println("Maior Height="+maior_height);
                Mat black = new Mat(quadrados_3.rows(),quadrados_3.cols(), CvType.CV_8UC1,new Scalar(0));
                Imgproc.rectangle(black, new Point(menor_x+maior_width,menor_y+maior_height),new Point(maior_x - maior_width,maior_y-maior_height),new Scalar(255),-1);

                float value_pixel =(float) (9.0/(bigger_area/4.0));
                Mat crop = new Mat();
                //CROP INSIDE
                System.out.println("Original: "+original.cols() + "x" +original.rows());

                original.copyTo(crop,black);
                Rect roi = new Rect(new Point(menor_x+maior_width,menor_y+maior_height),new Point(maior_x - maior_width,maior_y-maior_height));
                crop = new Mat(crop,roi);

                img_gui = Bitmap.createBitmap(crop.cols(), crop.rows(), Bitmap.Config.ARGB_8888);




                //Utils.matToBitmap(new_black,img_gui);
                Mat hsv_crop = new Mat();
                Imgproc.cvtColor(crop, hsv_crop, Imgproc.COLOR_RGB2HSV,3);
                Mat  new_s;
                //GET S inside
                List<Mat> hsv_channels_2 = new ArrayList<Mat>();
                Core.split(hsv_crop, hsv_channels_2);
                new_s = hsv_channels_2.get(1);


                ///Utils.matToBitmap(new_s,img_gui);
                Mat ret_s_crop = new Mat();
                //Utils.matToBitmap(new_s,img_gui);
                //Utils.matToBitmap(new_s,img_gui);
                Imgproc.threshold(new_s,new_s,0,255,Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
                //Utils.matToBitmap(new_s,img_gui);
                //Utils.matToBitmap(new_s,img_gui);
                Imgproc.threshold(new_s,ret_s_crop,0,255,Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);

                //ret_s_crop.copyTo(ret_s_crop,black);

                bmp = Bitmap.createBitmap(ret_s_crop.cols(), ret_s_crop.rows(), Bitmap.Config.ARGB_8888);


                Mat mask_crop = new Mat();
                ret_s_crop = bwareaopen(ret_s_crop, (int)(ret_s_crop.cols() * ret_s_crop.rows()*0.00005));
                mask_crop = ret_s_crop.clone();
                double area_folha = Core.countNonZero(ret_s_crop)*value_pixel;
                System.out.println("Área da folha = "+area_folha);
                List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                Imgproc.findContours(ret_s_crop,contours,new Mat(),Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);


                Point topMost = contours.get(0).toList().get(0);
                Point bottomMost = contours.get(0).toList().get(0);
                Point rightMost = contours.get(0).toList().get(0);
                Point leftMost =contours.get(0).toList().get(0);;
                int pixels_in_perimeter = 0;
                for( MatOfPoint mop: contours ){
                    for( Point p: mop.toList() ){
                        pixels_in_perimeter++;
                        if(p.y < topMost.y){
                            topMost = p;
                        }
                        if(p.y > bottomMost.y) {
                            bottomMost = p;
                        }
                    }
                }

                /*Imgproc.circle(crop,topMost,30,new Scalar(255,0,0),-1);
                Imgproc.circle(crop,bottomMost,30,new Scalar(0,255,0),-1);
                Imgproc.circle(crop,leftMost,30,new Scalar(0,0,255),-1);
                Imgproc.circle(crop,rightMost,30,new Scalar(0,255,255),-1);*/

                double m = ((bottomMost.y - topMost.y)/(bottomMost.x-topMost.x));
                double angulo = Math.toDegrees(Math.atan(m));
                if(angulo<0){
                    angulo = 90-(-angulo);
                }else{
                    angulo = angulo - 90;
                }
                System.out.println("M = "+m);
                System.out.println("Angulo = "+angulo);
                Mat outputImg = new Mat();
                crop.copyTo(outputImg,mask_crop);

                Mat matrix = Imgproc.getRotationMatrix2D(new Point(outputImg.cols()/2.0,outputImg.rows()/2.0),angulo,1);

                Imgproc.warpAffine(outputImg,outputImg,matrix,new Size(outputImg.cols(),outputImg.rows()));
                Imgproc.warpAffine(mask_crop,mask_crop,matrix,new Size(mask_crop.cols(),mask_crop.rows()));

                Mat mask_crop_copy = mask_crop.clone();
                contours = new ArrayList<MatOfPoint>();
                Imgproc.findContours(mask_crop_copy,contours,new Mat(),Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);

                topMost = contours.get(0).toList().get(0);
                bottomMost = contours.get(0).toList().get(0);
                rightMost = contours.get(0).toList().get(0);
                leftMost =contours.get(0).toList().get(0);;

                for( MatOfPoint mop: contours ){
                    for( Point p: mop.toList() ){
                        if(p.y < topMost.y){
                            topMost = p;
                        }
                        if(p.y > bottomMost.y){
                            bottomMost = p;
                        }
                        if(p.x < leftMost.x){
                            leftMost = p;
                        }
                        if(p.x > rightMost.x){
                            rightMost = p;
                        }
                    }
                }

                Imgproc.circle(outputImg,topMost,2,new Scalar(255,0,0),-1);
                Imgproc.circle(outputImg,bottomMost,2,new Scalar(0,255,0),-1);
                Imgproc.circle(outputImg,leftMost,2,new Scalar(0,0,255),-1);
                Imgproc.circle(outputImg,rightMost,2,new Scalar(0,255,255),-1);

                double altura = Math.sqrt(Math.pow(topMost.x- bottomMost.x,2)+Math.pow(topMost.y- bottomMost.y,2))*Math.sqrt(value_pixel);
                double largura = Math.sqrt(Math.pow(leftMost.x- rightMost.x,2))*Math.sqrt(value_pixel);
                Imgproc.rectangle(outputImg,new Point(leftMost.x, topMost.y),new Point(rightMost.x, bottomMost.y),new Scalar(255,0,0));
                System.out.println("Altura = "+altura);
                System.out.println("Largura = "+largura);

                //Desenhando Perimetro
                for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
                    Imgproc.drawContours(outputImg,contours,contourIdx,new Scalar(20,20,255),1);
                }

                Mat uncropped = outputImg.clone();
                Mat new_black = new Mat(1024,768, CvType.CV_8SC3,new Scalar(0));
                uncropped.copyTo(new_black);
                img_gui = Bitmap.createBitmap(new_black.cols(), new_black.rows(), Bitmap.Config.RGB_565);
                Utils.matToBitmap(new_black,img_gui);
                /*Rect roi_2 = new Rect(new Point(menor_x+maior_width,menor_y+maior_height),new Point(maior_x - maior_width,maior_y-maior_height));
                outputImg = new Mat(uncropped, roi_2);
                bmp = Bitmap.createBitmap(outputImg.cols(), outputImg.rows(), Bitmap.Config.RGB_565);*/

                double media = mean(areas);
                double sd = sd(areas,media);
                System.out.println("Media = "+media);
                System.out.println("Desvio padrão = "+sd);
                if(sd/media > 0.1){
                    Context context = getApplicationContext();
                    CharSequence text = "Imagem Inválida! A área entre os quadrados está diferente";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    return;
                }




                System.out.println("Terminou "+value_pixel+"cm^2");


                double perimetro = pixels_in_perimeter*Math.sqrt(value_pixel);

                height = altura;
                width = largura;
                leaf_area = area_folha;
                perimeter = perimetro;
               /* info1.setText("Altura: "+String.format("%.2f cm",height));
                info2.setText("Largura: "+String.format("%.2f cm",width));
                info3.setText("Área: "+String.format("%.2f cm²",leaf_area));
                info4.setText("Perimetro: "+String.format("%.2f cm",perimeter));
                process.setClickable(false);*/
                //Imgproc.putText(outputImg,"Height = "+String.format("%.2f cm",altura),new Point(10,30),Core.FONT_HERSHEY_PLAIN,2,new Scalar(255,0,0),2);
                //Imgproc.putText(outputImg,"Width = "+String.format("%.2f cm",largura),new Point(10,60),Core.FONT_HERSHEY_PLAIN,2,new Scalar(255,0,0),2);
                //Imgproc.putText(outputImg,"Area = "+String.format("%.2f cm²",area_folha),new Point(10,90),Core.FONT_HERSHEY_PLAIN,2,new Scalar(255,0,0),2);
                //Imgproc.putText(outputImg,"Perimetro = "+String.format("%.2f cm",perimetro),new Point(10,120),Core.FONT_HERSHEY_PLAIN,2,new Scalar(255,0,0),2);
                //Utils.matToBitmap(outputImg,bmp);
                //img_gui = bmp;
                processou = true;
                //imgPicture.setImageBitmap(bmp);
                //showMeasuares(v);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
   /* public void showMeasuares(View view) {
        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //define o titulo
        builder.setTitle("Medidas da folha");
        //define a mensagem
        builder.setMessage("Altura "+String.format("%.2f cm",height)+"\nLargura "+String.format("%.2f cm",width)+"\nÁrea "+String.format("%.2f cm²",leaf_area)+"\nPerimetro "+String.format("%.2f cm",perimeter));
        //define um botão como positivo


        //cria o AlertDialog
        alertDialog = builder.create();
        //Exibe
        alertDialog.show();
    }*/

    public void atualiza_ui(){
        if(processou) {
            process.setClickable(false);
            info1.setText("Altura: " + String.format("%.2f cm", height));
            info2.setText("Largura: " + String.format("%.2f cm", width));
            info3.setText("Área: " + String.format("%.2f cm²", leaf_area));
            info4.setText("Perimetro: " + String.format("%.2f cm", perimeter));
            imgPicture.setImageBitmap(img_gui);
        }

    }

    public void preCalcula(View view){
        final ProgressDialog myPd_ring=ProgressDialog.show(MainActivity.this, "Por favor espere", "Processando...", true);
        myPd_ring.setCancelable(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try
                {
                    Calcula(null);
                    Thread.sleep(1000);
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            atualiza_ui();
                        }
                    });
                }catch(Exception e){}
                myPd_ring.dismiss();
            }
        }).start();
        Calcula(view);


    }

    public void trocar(View v){
        processou = false;
        process.setClickable(true);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        // finally, get a URI representation
        Uri data = Uri.parse(pictureDirectoryPath);
        // set the data and type.  Get all image types.
        photoPickerIntent.setDataAndType(data, "image/*");
        // we will invoke this activity, and get something back from it.
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);
        
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // if we are here, everything processed successfully.
            if (requestCode == IMAGE_GALLERY_REQUEST) { //Galeria

                Uri imageUri = data.getData();
                imgPicture.setImageBitmap(getImage(imageUri));
                Calendar c = Calendar.getInstance();
                int Month = c.get(Calendar.MONTH) + 1;
                String mes;
                if(Month < 10){
                    mes = "0"+Integer.toString(Month);
                }else{
                    mes = Integer.toString(Month);
                }
                String date = c.get(Calendar.DAY_OF_MONTH)+ "/"+ mes + "/" + c.get(Calendar.YEAR);
                info1.setText(date);
                Bundle b = getIntent().getExtras();
                if(b != null) {
                    String scheme = imageUri.getScheme();
                    if (scheme.equals("file")) {
                        name.setText(imageUri.getLastPathSegment());
                    }else{
                        String[] proj = { MediaStore.Images.Media.TITLE };
                        Cursor cursor = this.getApplicationContext().getContentResolver().query(imageUri, proj, null, null, null);
                        if (cursor != null && cursor.getCount() != 0) {
                            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.TITLE);
                            cursor.moveToFirst();
                            name.setText(cursor.getString(columnIndex));
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                        //name.setText("Error with file name");
                    }
                }
                info3.setText("");
                info4.setText("");
            }
            if (requestCode == CAM_REQUEST) { //Camera
                // imgPicture.setImageURI(mHighQualityImageUri);
                /*System.out.println("ou aqui?");
                /*Toast.makeText(this, "Image saved to:\n" +
                        data.getData(), Toast.LENGTH_LONG).show();
                System.out.println("antes uri");
                Uri imageUri = fileUri;

                // declare a stream to read the image data from the SD Card.
                InputStream inputStream;
                try {
                    System.out.println("Dentro do try");
                    inputStream = getContentResolver().openInputStream(imageUri);
                    System.out.println("Antes bitmap");
                    // get a bitmap from the stream.

                    Bitmap image = BitmapFactory.decodeStream(inputStream);
                    //Bitmap b = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
                    Matrix m = new Matrix();
                    m.setRectToRect(new RectF(0, 0, image.getWidth(), image.getHeight()), new RectF(0, 0, 768, 768), Matrix.ScaleToFit.CENTER);
                    image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), m, true);
                    System.out.println("antes do set");

                    // show the image to the user
                    imgPicture.setImageBitmap(image);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    // show a message to the user indictating that the image is unavailable.
                    Toast.makeText(this, "Unable to open image", Toast.LENGTH_LONG).show();
                }*/
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.gabriel_jonas.leaf_segmentation/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.gabriel_jonas.leaf_segmentation/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
